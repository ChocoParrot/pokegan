import tensorflow as tf
import numpy as np

from model import Model
from configreader import read
import datahandler as image

def createSession ():
    session = tf.Session()
    session.run(tf.global_variables_initializer())
    return session

def saveModel (sess):
    saver = tf.train.Saver()
    saver.save(sess, config["directories"]["checkpoints"] + "/" + config["run-name"])

config = read()
model = Model(config["training"]["batch-size"])

randoms = np.random.rand(config["training"]["collage-size"], 600)

with createSession() as session:
    itx = 0

    # initialise TensorBoard summaries
    tf.summary.scalar("Generator-Loss", model.loss_generator)
    tf.summary.scalar("Discriminator-Loss", model.loss_discriminator)
    tf.summary.image("Input-Pokemon", model.input_discriminator)
    tf.summary.image("Generated-Fakemon", model.output_generator)

    merged = tf.summary.merge_all()
    writer = tf.summary.FileWriter(config["directories"]["tensorboard"] + "/" + config["run-name"], session.graph)

    getImgs_thread = image.startGetImages(config["training"]["batch-size"])

    while True:
        feed = {model.input_generator: np.random.rand(config["training"]["batch-size"], 600), model.input_discriminator: image.stopGetImages(getImgs_thread)}
        getImgs_thread = image.startGetImages(config["training"]["batch-size"])

        if (itx % config["training"]["collage-every"] == 0):
            images = session.run(model.output_generator_raw, feed_dict={model.input_generator: randoms})
            image.saveImagesArray(images, config["training"]["collage-dims"][0], config["training"]["collage-dims"][1], config["directories"]["collage"] + "/" + str(itx) + ".png")

        # model.optimiser_discriminator, model.optimiser_generator, model.optimiser_generator
        summary, _, _, _ = session.run([merged, model.optimiser_discriminator, model.optimiser_generator, model.optimiser_generator], feed_dict=feed)


        writer.add_summary(summary, itx)

        if (itx % config["training"]["save-every"] == 0):
            print("Attempting save...")
            saveModel(session)
            print("Save complete!")

        itx += 1
