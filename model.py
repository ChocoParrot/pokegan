import tensorflow as tf

class Model():
    def __init__ (self, constant_size):
        self.input_generator = tf.placeholder(tf.float32, [None, 600])
        self.input_discriminator = tf.placeholder(tf.float32, [None, 30, 40, 4])

        def clipGradients (x, loss, vars, value):
            grads = x.compute_gradients(loss, var_list=vars)
            clipped = [(tf.clip_by_value(gradx, -value, value), var) for gradx, var in grads]
            opt = x.apply_gradients(clipped)
            return opt

        def deconvolutions (x):
            with tf.variable_scope("generator", reuse=tf.AUTO_REUSE), tf.name_scope("deconvolutions"):
                deconv1 = tf.layers.conv2d_transpose(x, 512, 5, strides=2, activation=tf.nn.leaky_relu, padding="valid")
                deconv2 = tf.layers.conv2d_transpose(deconv1, 256, 5, strides=2, activation=tf.nn.leaky_relu, padding="valid")
                deconv3 = tf.layers.conv2d_transpose(deconv2, 128, 5, strides=2, activation=tf.nn.leaky_relu, padding="valid")
                deconv4 = tf.layers.conv2d_transpose(deconv3, 64, 5, strides=2, activation=tf.nn.leaky_relu, padding="valid")
                deconv5 = tf.layers.conv2d_transpose(deconv4, 4, 5, strides=2, activation=tf.sigmoid, padding="valid")

                resize = tf.image.resize_images(deconv5, [30, 40])

            return resize

        def convolutions (x):
            with tf.variable_scope("discriminator", reuse=tf.AUTO_REUSE), tf.name_scope("convolutions"):
                conv1 = tf.layers.conv2d(x, 64, 5, strides=1, activation=None)
                pool1 = tf.layers.average_pooling2d(conv1, 2, strides=1, padding="same")
                conv2 = tf.layers.conv2d(conv1, 128, 5, strides=1, activation=tf.nn.leaky_relu)
                pool2 = tf.layers.average_pooling2d(conv2, 2, strides=1, padding="same")
                conv3 = tf.layers.conv2d(conv2, 256, 5, strides=1, activation=tf.nn.leaky_relu)
                pool3 = tf.layers.average_pooling2d(conv3, 2, strides=1, padding="same")
                conv4 = tf.layers.conv2d(conv3, 512, 5, strides=1, activation=tf.nn.leaky_relu)
                pool4 = tf.layers.average_pooling2d(conv4, 3, strides=1, padding="same")
                conv5 = tf.layers.conv2d(conv4, 1, 5, strides=1, activation=tf.nn.leaky_relu)
                pool5 = tf.layers.average_pooling2d(conv5, 9, strides=1, padding="same")

            return pool5

        def feedforward (x):
            with tf.variable_scope("discriminator", reuse=tf.AUTO_REUSE), tf.name_scope("feedforward"):
                flatten = tf.layers.flatten(x)
                dense = tf.layers.dense(flatten, 2, activation=tf.nn.softmax)

            return dense

        def discriminator (x):
            return feedforward(convolutions(x))

        def cross_entropy(y, x):
            return tf.reduce_mean(-tf.reduce_sum(y * tf.log(x), 1))

        const_real = tf.constant([1, 0], shape=[constant_size, 2], dtype=tf.float32)
        const_fake = tf.constant([0, 1], shape=[constant_size, 2], dtype=tf.float32)

        # reshape
        reshaped_input_generator = tf.reshape(self.input_generator, [-1, 4, 3, 50])
        normalised_input_discriminator = tf.scalar_mul(1/255, self.input_discriminator)

        # generator output
        output_generator = tf.scalar_mul(255, deconvolutions(reshaped_input_generator))
        self.output_generator = tf.cast(output_generator, tf.uint8)
        self.output_generator_raw = output_generator

        self.output_generator_discriminator = discriminator(deconvolutions(reshaped_input_generator))
        self.output_discriminator = discriminator(normalised_input_discriminator)

        # losses
        self.loss_discriminator = cross_entropy(const_real, self.output_discriminator) + cross_entropy(const_fake, self.output_generator_discriminator)
        self.loss_generator = cross_entropy(const_real, self.output_generator_discriminator)

        # variables
        gen_variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="generator")
        discrim_variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="discriminator")

        # optimisers
        self.optimiser_discriminator = clipGradients(tf.train.RMSPropOptimizer(learning_rate=5e-4, momentum=0.05, decay=0.92), self.loss_discriminator, discrim_variables, 5)
        self.optimiser_generator = clipGradients(tf.train.RMSPropOptimizer(learning_rate=1e-4, momentum=0.05, decay=0.92), self.loss_generator, gen_variables, 5)
