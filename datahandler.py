import imageio
import numpy as np
import random
import os

from configreader import read

import threading

# code by Arduano

config = read()

data_dir_list = os.listdir(config["directories"]["data"])

class GetImagesThread (threading.Thread):
    def __init__(self, amount):
        threading.Thread.__init__(self)
        self.amount = amount

    def run(self):
        ret = np.zeros([0, 30, 40, 4])
        i = 0
        while i < self.amount:
            try:
                image = random.choice(data_dir_list)
                appendable = np.expand_dims(imageio.imread(os.path.join(config["directories"]["data"], image)), 0)

                ret = np.concatenate([ret, appendable], 0)
                i += 1
            except:
                print("Error in loading image.")
                continue

        self.ret = ret

def startGetImages(amount):
    t = GetImagesThread(amount)
    t.start()
    return t

def stopGetImages(t):
    t.join()
    ret = t.ret
    t._delete()
    return ret

class SaveThread (threading.Thread):
    def __init__(self, imgs, dimx, dimy, path):
        threading.Thread.__init__(self)
        self.imgs = imgs
        self.dimx = dimx
        self.dimy = dimy
        self.path = path

    def run(self):
        img = np.zeros([self.imgs.shape[1] * self.dimx, 0, 4])
        for i in range(self.dimy):
            line = np.zeros([0, self.imgs.shape[2], 4])
            for j in range(self.dimx):
                line = np.concatenate([line, self.imgs[i * self.dimx + j]], 0)
            img = np.concatenate([img, line], 1)
        img = np.multiply(img, 255)
        imh = img.astype('uint8')
        imageio.imsave(self.path, img)

def saveImagesArray(imgs, dimx, dimy, path):
    thread = SaveThread(imgs, dimx, dimy, path)
    thread.run()
