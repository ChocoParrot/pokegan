import json
import os

def read ():

    path = os.path.dirname(os.path.abspath(__file__))

    f = open(path + "/config.json", "r")

    return json.load(f)
